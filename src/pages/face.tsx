import React, {useEffect, useState} from "react";
import {Center, Text,} from "native-base";
import Script from 'next/script';
import {completeFaceSessionWithRetries, getFaceTecSessionToken, initFaceTec} from "../facetec/facetecActions";


export default function Face() {

  const [status, setStatus] = useState('initial');
  const [sessionToken, setSessionToken] = useState('');

  /* executed once the script is loaded */
  const init = () => {
    setStatus('initializing');
    initFaceTec().then(initialized => {
      setStatus(initialized ? 'initialized' : 'init failed');
    });
  };

  useEffect(() => {
    if (status !== 'initialized') return;
    setStatus('starting session');
    getFaceTecSessionToken().then((sessionToken) => {
      setSessionToken(sessionToken);
      console.log('sessionToken', sessionToken);
      setStatus('session started');
    }).catch(e => {
      setStatus('session failed to start');
      console.log(e);
    });
  }, [status]);

  useEffect(() => {
    if (status !== 'session started') return;
    setStatus('running');
    completeFaceSessionWithRetries(sessionToken)
        .then(status => setStatus(status))
        .catch(e => {
          setStatus('failed');
          console.log(e);
        });
  }, [status, sessionToken])


  return (
    <Center
      flex={1}
      _dark={{ bg: "blueGray.900" }}
      _light={{ bg: "blueGray.50" }}
    >
      <Script src="/core-sdk/FaceTecSDK.js/FaceTecSDK.js" strategy="afterInteractive" onLoad={init} />
      <Text fontSize="xl">{status}</Text>
    </Center>
  );
}
