import type {
  FaceTecFaceScanProcessor,
  FaceTecFaceScanResultCallback,
  FaceTecSessionResult
} from '../core-sdk/FaceTecSDK.js/FaceTecPublicApi';
import {Observable} from '../utils/observable';
import type {FaceTecSDK as FTSDK} from '../core-sdk/FaceTecSDK.js/FaceTecSDK';
import {deviceKey, encryptionKey} from './config';
import * as facetecApi from "./facetecApi";

// @ts-ignore
const getFaceTecSDK = (): FTSDK => FaceTecSDK;

export const initFaceTec = async (): Promise<boolean> => {
    const FaceTecSDK = getFaceTecSDK();
    if (FaceTecSDK.getStatus() === FaceTecSDK.FaceTecSDKStatus.Initialized) {
        return true;
    }
    FaceTecSDK.setImagesDirectory('/core-sdk/FaceTec_images');
    FaceTecSDK.setResourceDirectory('/core-sdk/FaceTecSDK.js/resources');
    return new Promise<boolean>(
        resolve => FaceTecSDK.initializeInDevelopmentMode(deviceKey, encryptionKey, resolve)
    );
};

export const getFaceTecSessionToken = async () => {
    const FaceTecSDK = getFaceTecSDK();
    const userAgent = FaceTecSDK.createFaceTecAPIUserAgentString('');
    return facetecApi.getSession(userAgent).then(response => response.json()).then(json => json.sessionToken);
};

export type FaceScanResult = {
    sessionResult: FaceTecSessionResult;
    faceScanResultCallback: FaceTecFaceScanResultCallback;
};

const startFaceCapture = (sessionToken: string, observable: Observable<FaceScanResult>) => {
    const FaceTecSDK = getFaceTecSDK();
    const callbacks: FaceTecFaceScanProcessor = {
        processSessionResultWhileFaceTecSDKWaits: (sessionResult, faceScanResultCallback) => {
            console.log('processSessionResultWhileFaceTecSDKWaits');
            observable.nextValue({sessionResult, faceScanResultCallback});
        },
        onFaceTecSDKCompletelyDone: () => {
            /* do nothing */
            console.log('onFaceTecSDKCompletelyDone');
        }
    };
    new FaceTecSDK.FaceTecSession(callbacks, sessionToken);
};

// export const uploadFace = async (
//     sessionResult: FaceTecSessionResult,
//     faceScanResultCallback: FaceTecFaceScanResultCallback,
// ): Promise<{ status: string }> => {
//     const FaceTecSDK = getFaceTecSDK();
//     const faceData = {
//         auditTrailImage: sessionResult.auditTrail[0],
//         lowQualityAuditTrailImage: sessionResult.lowQualityAuditTrail[0],
//         faceScan: sessionResult.faceScan,
//     };
//     const userAgent = FaceTecSDK.createFaceTecAPIUserAgentString(sessionResult.sessionId as string);
//     return facetecApi.enroll3d(faceData, userAgent, faceScanResultCallback.uploadProgress)
//         .then(({wasProcessed, success, scanResultBlob}) => {
//             if (wasProcessed) {
//                 faceScanResultCallback.proceedToNextStep(scanResultBlob);
//             } else {
//                 faceScanResultCallback.cancel();
//             }
//             let status: string;
//             if (wasProcessed && success) {
//                 status = 'success';
//             } else if (wasProcessed) {
//                 status = 'retry needed';
//             } else {
//                 status = 'failed';
//             }
//             return {status};
//         }).catch(() => {
//             faceScanResultCallback.cancel();
//             return {status: 'upload error'};
//         });
// };

export const completeFaceSessionWithRetries = async (sessionToken: string): Promise<string> => {
    const FaceTecSDK = getFaceTecSDK();
    const sessionObservable = new Observable<FaceScanResult>();
    startFaceCapture(sessionToken, sessionObservable);
    let tries = 1;
    const maxTries = 3;
    while (tries <= maxTries) {
        const {sessionResult, faceScanResultCallback} = await sessionObservable.waitForNext();
        if (sessionResult.status !== FaceTecSDK.FaceTecSessionStatus.SessionCompletedSuccessfully) {
            faceScanResultCallback.cancel();
            return 'failed';
        }
        // skipping upload for now
        faceScanResultCallback.succeed();
        return 'succeeded';
        // const {status: uploadStatus} = await uploadFace(
        //     sessionResult,
        //     faceScanResultCallback,
        // );
        // switch (uploadStatus) {
        //     case 'retry needed':
        //         tries += 1;
        //         if (tries > maxTries) {
        //             faceScanResultCallback.cancel();
        //         }
        //         break;
        //     case 'success':
        //         return 'succeeded';
        //     case 'failed':
        //     default:
        //         faceScanResultCallback.cancel();
        //         return 'failed';
        // }
    }
    return 'failed';
}
