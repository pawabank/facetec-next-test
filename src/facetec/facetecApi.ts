import {baseUrl, deviceKey} from "./config";

const fetchWithProgress = (
    url: string,
    opts: RequestInit & { onProgress?: (p: number) => void } = {}
): Promise<XMLHttpRequest> => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'get', url);
        const headers = (opts.headers || {}) as Record<string, string>;
        Object.keys(headers).forEach(k => xhr.setRequestHeader(k, headers[k]));
        if (opts.credentials === 'include') {
            xhr.withCredentials = true;
        }
        xhr.onload = e => resolve(e.target as XMLHttpRequest);
        xhr.onerror = reject;
        if (xhr.upload && opts.onProgress) xhr.upload.onprogress = event => opts.onProgress?.(event.loaded / event.total);
        xhr.send(JSON.stringify(opts.body));
    });
};

export const getSession = async (userAgent: string): Promise<any> => {
    return fetch(`${baseUrl}/session-token`, {
        headers: {
            'X-Device-Key': deviceKey,
            'X-User-Agent': userAgent
        }
    });
}

// export const enroll3d = async (data: any, userAgent: string, onProgress: (p: number) => void): Promise<any> => {
//     return fetchWithProgress(`${baseUrl}/enrollment-3d`, {
//         method: 'put',
//         headers: { 'X-User-Agent': userAgent, 'Content-Type': 'application/json' },
//         body: data,
//         onProgress
//     }).then(response => JSON.parse(response.response));
// }
