## Running the example

- update the FaceTec config in `src/facetec/config.ts`
- run `yarn` / `npm install`
- run `yarn dev` / `npm run dev`
- open [http://localhost:3000](http://localhost:3000)
- press the `Test <Script> + FaceTec` button
